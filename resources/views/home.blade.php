@extends('master')

@section('css')
<link rel="stylesheet" href="{{ asset(mix('/css/app.css')) }}">
@endsection

@section('content')
<div id='home'>
    <home/>
</div>
@endsection

@section('js')
    <script src="{{ asset(mix('js/app.js')) }}"></script>
@endsection

    