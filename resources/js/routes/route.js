import VueRouter from 'vue-router'
import Vue from 'vue'

import NotFound from '../layouts/NotFound'
import Ingredient from '../pages/Ingredient'
import Recipe from '../pages/Recipe'
import Box from '../pages/Box'
import Order from '../pages/Order'

Vue.use(VueRouter)

const router = new VueRouter({
    linkActiveClass: "active",
    mode: 'history',
    routes: [
        {
            path: '*',
            component: NotFound,
            name: 'not-found',
        },
        {
            path: '/ingredients',
            name: 'ingredients',
            component: Ingredient,
            meta: {
                title: 'Ingredients',
            },
        },
        {
            path: '/recipes',
            name: 'recipes',
            component: Recipe,
            meta: {
                title: 'Recipes',
            },
        },
        {
            path: '/boxes',
            name: 'boxes',
            component: Box,
            meta: {
                title: 'Boxes',
            },
        },
        {
            path: '/orders',
            name: 'orders',
            component: Order,
            meta: {
                title: 'Order',
            },
        },
    ]

})


export default router