import Vue from 'vue'
import axios from "axios";
import router from './routes/route';
import store from './store/index';
import Home from './pages/Home';

import VModal from 'vue-js-modal'
Vue.use(VModal)

import Vuetify from 'vuetify';
Vue.use(Vuetify);

import 'vue-datetime/dist/vue-datetime.css'
import { Datetime } from 'vue-datetime';
Vue.component('datetime', Datetime);

Vue.mixin(require('./asset'));

new Vue({
  el: "#home",
  router,
  store,
  VModal,
  vuetify: new Vuetify({
    theme: {
      themes: {
        light: {
          primary: "#48AEFF",
        },
      }
    }
  }),
  components: {
    Home
  }
})