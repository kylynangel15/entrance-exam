import Vue from 'vue'
import Vuex from 'vuex'
import ingredients from './modules/ingredients'
import notification from './modules/notification'
import recipes from './modules/recipes'
import boxes from './modules/boxes'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        notification,
        ingredients,
        recipes,
        boxes
    }
})
