import axios from 'axios'
import Swal from 'sweetalert2'

const data = {
    namespaced: true,
    actions: {
        succesNotif({ commit }, message){
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              })
              
              Toast.fire({
                icon: 'success',
                title: message == null ? 'Data has been saved' : message  
              })
        },
        errorNotif({ commit }, message){
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              })
              
              Toast.fire({
                icon: 'error',
                title: message == null ? 'Something went wrong!' : message  
              })
        },
        questionNotif({ commit }, message){
            return Swal.fire({
                title: message == null ? 'Are you sure? ' : message.title,
                text: message == null ? '' : message.text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: message == null ? 'Yes!' : message.confirmButtonText
                }).then((result) => {
                    return result.value
                })
        },
        warningNotif({ commit }, message){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: message,
              })
        },
        infoNotif({ commit }, message){
          Swal.fire({
              icon: 'info',
              title: 'Hello!',
              text: message,
            })
        },
    },
}

export default data;