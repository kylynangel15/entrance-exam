import axios from 'axios'
import router from '../../routes/route' 

function csrf_token() {
    return  axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': window.csrf_token
    };
}
const data = {
    namespaced: true,
    state: {
        ingredient_list: [],
        ingredient: [],
        order_ingredients: [],
        order_recipes: [],
    },
    getters: {
    },
    mutations: {
        SET_INGREDIENT_LIST: (state, payload) => {
            state.ingredient_list = payload
        },
        SET_INGREDIENT: (state, payload) => {
            state.ingredient = payload
        },
        SET_ORDER_FILTER: (state, payload) => {
            state.order_ingredients = payload.ingredients
            state.order_recipes = payload.recipes
        },
    },
    actions: {
        getList ( {commit} ) {      
            return axios
                .get( route('api.ingredients.index'))
                .then((response) => {
                    commit('SET_INGREDIENT_LIST', response.data)
                })
                .catch((error) => {
                    console.log(error)
                })
        },

        store ( {commit, dispatch}, payload ) {  
            csrf_token()
            return new Promise((resolve, reject) => {               
                axios
                .post( route('api.ingredients.store'), payload)
                .then((response) => {
                    if (response.status == 200){
                        dispatch('notification/succesNotif','Ingredient has been created', { root: true }); 
                        dispatch('getList')
                        resolve(true)
                    }else {
                        dispatch('notification/errorNotif', null, { root: true }); 
                    }
                })
                .catch((error) => {
                    if (error.response.status == 422){
                        reject(error.response.data.errors)
                    }
                })
            })
        },

        update ( {commit, dispatch}, payload ) {  
            csrf_token()

            return new Promise((resolve, reject) => {               
                axios
                .put( route('api.ingredients.update', payload.id), payload)
                .then((response) => {
                    if (response.status == 200){
                        dispatch('notification/succesNotif', 'Ingredient has been update', { root: true }); 
                        dispatch('getList')
                    }else {
                        dispatch('notification/errorNotif', null, { root: true }); 
                    }
                })
                .catch((error) => {
                    if (error.response.status == 422){
                        reject(error.response.data.errors)
                    }
                })
            })
        },

        destroy ( { commit,dispatch }, payload ) { 
            csrf_token()
            let msg = {
                title: "Are you sure?",
                confirmButtonText: "Yes, Remove!",
            }
            
            dispatch('notification/questionNotif', msg, { root: true })
            .then((result) => {
                if (result) { 
                    axios 
                    .delete( route('api.ingredients.destroy', payload.id))
                    .then((response) => {
                        dispatch('notification/succesNotif', 'Ingredient has been removed', { root: true }); 
                        dispatch('getList')
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                } 

            })
        },
        orderFilter ( {commit},payload ) {      
            return axios
                .get( route('api.order-filter'),{
                    params: {
                        order_date: payload,
                    }
                })
                .then((response) => {
                    commit('SET_ORDER_FILTER', response.data)
                })
                .catch((error) => {
                    console.log(error)
                })
        },
    },
}

export default data;