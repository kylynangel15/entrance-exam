import axios from 'axios'
import router from '../../routes/route' 

function csrf_token() {
    return  axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': window.csrf_token
    };
}
const data = {
    namespaced: true,
    state: {
        box_list: [],
        box: [],
    },
    getters: {
    },
    mutations: {
        SET_BOX_LIST: (state, payload) => {
            state.box_list = payload
        },
        SET_BOX: (state, payload) => {
            state.box = payload
        },
    },
    actions: {
        getList ( {commit}, payload ) {      
            return axios
                .get( route('api.boxes.index'), {
                    params: {
                        date_range: payload,
                    }
                })
                .then((response) => {
                    commit('SET_BOX_LIST', response.data)
                })
                .catch((error) => {
                    console.log(error)
                })
        },

        store ( {commit, dispatch}, payload ) {  
            csrf_token()
            return new Promise((resolve, reject) => {               
                axios
                .post( route('api.boxes.store'), payload)
                .then((response) => {
                    if (response.status == 200){
                        dispatch('notification/succesNotif','Box has been created', { root: true }); 
                        dispatch('getList')
                        resolve(true)
                    }else {
                        dispatch('notification/errorNotif', null, { root: true }); 
                    }
                })
                .catch((error) => {
                    if (error.response.status == 422){
                        dispatch('notification/errorNotif', 'Invalid Data, please check ', { root: true }); 
                        reject(error.response.data.errors)
                    }
                })
            })
        },

        update ( {commit, dispatch}, payload ) {  
            csrf_token()

            return new Promise((resolve, reject) => {               
                axios
                .put( route('api.boxes.update', payload.id), payload)
                .then((response) => {
                    if (response.status == 200){
                        dispatch('notification/succesNotif', 'Box has been update', { root: true }); 
                        dispatch('getList')
                    }else {
                        dispatch('notification/errorNotif', null, { root: true }); 
                    }
                })
                .catch((error) => {
                    if (error.response.status == 422){
                        reject(error.response.data.errors)
                    }
                })
            })
        },

        destroy ( { commit,dispatch }, payload ) { 
            csrf_token()
            let msg = {
                title: "Are you sure?",
                confirmButtonText: "Yes, Remove!",
            }
            
            dispatch('notification/questionNotif', msg, { root: true })
            .then((result) => {
                if (result) { 
                    axios 
                    .delete( route('api.boxes.destroy', payload.id))
                    .then((response) => {
                        dispatch('notification/succesNotif', 'Box has been removed', { root: true }); 
                        dispatch('getList')
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                } 

            })
        },
    },
}

export default data;