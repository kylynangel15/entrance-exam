import axios from 'axios'
import router from '../../routes/route' 

function csrf_token() {
    return  axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': window.csrf_token
    };
}
const data = {
    namespaced: true,
    state: {
        recipe_list: [],
        recipes: [],
    },
    getters: {
    },
    mutations: {
        SET_RECIPE_LIST: (state, payload) => {
            state.recipe_list = payload
        },
        SET_RECIPES: (state, payload) => {
            state.recipes = payload
        },
    },
    actions: {
        getList ( {commit} ) {      
            return axios
                .get( route('api.recipes.index'))
                .then((response) => {
                    commit('SET_RECIPE_LIST', response.data)
                })
                .catch((error) => {
                    console.log(error)
                })
        },

        store ( {commit, dispatch}, payload ) {  
            csrf_token()
            console.log(payload);
            return new Promise((resolve, reject) => {               
                axios
                .post( route('api.recipes.store'), payload)
                .then((response) => {
                    if (response.status == 200){
                        dispatch('notification/succesNotif','Recipe has been created', { root: true }); 
                        dispatch('getList')
                        resolve(true)
                    }else {
                        dispatch('notification/errorNotif', null, { root: true }); 
                    }
                })
                .catch((error) => {
                    if (error.response.status == 422){
                        if(error.response.data.errors.ingredients)
                            dispatch('notification/errorNotif', 
                                    error.response.data.errors.ingredients[0], 
                                    { root: true }
                                ); 
                        else {
                            dispatch('notification/errorNotif', 'Invalid Data, please check ', { root: true }); 
                        }
                        
                        reject(error.response.data.errors)
                    }
                })
            })
        },

        update ( {commit, dispatch}, payload ) {  
            csrf_token()

            return new Promise((resolve, reject) => {               
                axios
                .put( route('api.recipes.update', payload.id), payload)
                .then((response) => {
                    if (response.status == 200){
                        dispatch('notification/succesNotif', 'Recipe has been update', { root: true }); 
                        dispatch('getList')
                    }else {
                        dispatch('notification/errorNotif', null, { root: true }); 
                    }
                })
                .catch((error) => {
                    if (error.response.status == 422){
                        if(error.response.data.errors.ingredients)
                            dispatch('notification/errorNotif', 
                                    error.response.data.errors.ingredients[0], 
                                    { root: true }
                                ); 
                        else {
                            dispatch('notification/errorNotif', 'Invalid Data, please check ', { root: true }); 
                        }
                        
                        reject(error.response.data.errors)
                    }
                })
            })
        },

        destroy ( { commit,dispatch }, payload ) { 
            csrf_token()
            let msg = {
                title: "Are you sure?",
                confirmButtonText: "Yes, Remove!",
            }
            
            dispatch('notification/questionNotif', msg, { root: true })
            .then((result) => {
                if (result) { 
                    axios 
                    .delete( route('api.recipes.destroy', payload.id))
                    .then((response) => {
                        dispatch('notification/succesNotif', 'Recipe has been removed', { root: true }); 
                        dispatch('getList')
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                } 

            })
        },
    },
}

export default data;