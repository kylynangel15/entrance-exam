<?php

use Illuminate\Database\Seeder;
use App\Models\Box;

class BoxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Box::truncate();
    	Box::insert($this->data());
        DB::table('recipe_boxes')->insert($this->dataPivot());
    }
    private function dataPivot()
    {
        return [
            [
                'box_id'    => 1,
                'recipe_id' => 5,
            ],
            [
                'box_id'    => 1,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 1,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 3,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 3,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 4,
                'recipe_id' => 1,
            ],
            [
                'box_id'    => 4,
                'recipe_id' => 2,
            ],
            [
                'box_id'    => 4,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 4,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 5,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 5,
                'recipe_id' => 2,
            ],
            [
                'box_id'    => 6,
                'recipe_id' => 1,
            ],
            [
                'box_id'    => 7,
                'recipe_id' => 2,
            ],
            [
                'box_id'    => 7,
                'recipe_id' => 1,
            ],
            [
                'box_id'    => 8,
                'recipe_id' => 5,
            ],
            [
                'box_id'    => 8,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 8,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 9,
                'recipe_id' => 2,
            ],
            [
                'box_id'    => 9,
                'recipe_id' => 1,
            ],
            [
                'box_id'    => 9,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 10,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 10,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 10,
                'recipe_id' => 2,
            ],
            [
                'box_id'    => 11,
                'recipe_id' => 5,
            ],
            
            [
                'box_id'    => 12,
                'recipe_id' => 4,
            ],
            [
                'box_id'    => 13,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 14,
                'recipe_id' => 3,
            ],
            [
                'box_id'    => 14,
                'recipe_id' => 2,
            ],
        ];
    }
    private function data()
    {
        return [
            [
                'delivery_date' => '2021-02-28 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-02 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-01 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-03 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-04 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-04 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-05 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-06 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-07 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-08 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-09 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-10 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-11 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'delivery_date' => '2021-03-12 08:00:00',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        ];
    }
}
