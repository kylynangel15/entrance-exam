<?php

use Illuminate\Database\Seeder;
use App\Models\Ingredient;

class IngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ingredient::truncate();
    	Ingredient::insert($this->data());
    }
    private function data()
    {
        return [
            [
                'name'       => 'Chicken, Thigh fillet',
                'measure'    => 'g',
                'supplier'   => 'Walmart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "DeL monte Quick 'n Easy BBQ Marinade",
                'measure'    => 'ml',
                'supplier'   => 'Stop & Shop',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Onion Red",
                'measure'    => 'g',
                'supplier'   => 'Walmart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Lemongrass Chopped",
                'measure'    => 'cup',
                'supplier'   => 'ShopRite',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Pepper Black",
                'measure'    => 'tsp',
                'supplier'   => 'Stop & Shop',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Cornstarch",
                'measure'    => 'tsp',
                'supplier'   => 'Winco Foods',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Contadina Pure Olive Oil",
                'measure'    => 'tbsp',
                'supplier'   => 'Walmart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Garlic",
                'measure'    => 'cup',
                'supplier'   => 'Stop & Shop',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Rice",
                'measure'    => 'cup',
                'supplier'   => 'SM Grocery Store',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Shrimp",
                'measure'    => 'g',
                'supplier'   => 'Walmart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Honey",
                'measure'    => 'cup',
                'supplier'   => 'Stop & Shop',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Del Monte Quick and Easy Curry Flavor",
                'measure'    => 'g',
                'supplier'   => 'Stop & Shop',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Pork",
                'measure'    => 'g',
                'supplier'   => 'Walmart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Beef",
                'measure'    => 'g',
                'supplier'   => 'Stop & Shop',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'       => "Parsley",
                'measure'    => 'ml',
                'supplier'   => 'ShopRite',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];
    }
}
