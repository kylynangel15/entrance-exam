<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(IngredientsSeeder::class);
        $this->call(RecipesSeeder::class);
        $this->call(BoxesSeeder::class);
    }
}
