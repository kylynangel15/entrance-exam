<?php

use Illuminate\Database\Seeder;
use App\Models\Recipe;
class RecipesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Recipe::truncate();
    	Recipe::insert($this->data());
        DB::table('ingredient_recipes')->insert($this->dataPivot());
    }
    private function dataPivot()
    {
        return [
            [
                'ingredient_id' => 1,
                'recipe_id'     => 1,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 2,
                'recipe_id'     => 1,
                'amount'        => 200,
            ],
            [
                'ingredient_id' => 3,
                'recipe_id'     => 1,
                'amount'        => 40,
            ],
            [
                'ingredient_id' => 4,
                'recipe_id'     => 1,
                'amount'        => 1,
            ],
            [
                'ingredient_id' => 5,
                'recipe_id'     => 1,
                'amount'        => 1,
            ],
            [
                'ingredient_id' => 6,
                'recipe_id'     => 1,
                'amount'        => 1,
            ],
            [
                'ingredient_id' => 7,
                'recipe_id'     => 1,
                'amount'        => 2,
            ],
            [
                'ingredient_id' => 8,
                'recipe_id'     => 1,
                'amount'        => 1,
            ],
            [
                'ingredient_id' => 9,
                'recipe_id'     => 1,
                'amount'        => 3,
            ],
            [
                'ingredient_id' => 10,
                'recipe_id'     => 2,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 8,
                'recipe_id'     => 2,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 2,
                'recipe_id'     => 2,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 7,
                'recipe_id'     => 2,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 12,
                'recipe_id'     => 3,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 7,
                'recipe_id'     => 3,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 1,
                'recipe_id'     => 3,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 6,
                'recipe_id'     => 3,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 11,
                'recipe_id'     => 3,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 2,
                'recipe_id'     => 4,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 11,
                'recipe_id'     => 4,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 5,
                'recipe_id'     => 4,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 13,
                'recipe_id'     => 4,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 14,
                'recipe_id'     => 5,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 3,
                'recipe_id'     => 5,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 8,
                'recipe_id'     => 5,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 12,
                'recipe_id'     => 5,
                'amount'        => 160,
            ],
            [
                'ingredient_id' => 15,
                'recipe_id'     => 5,
                'amount'        => 160,
            ],
        ];
    }
    private function data()
    {

        return [
            [
                'name'        => 'Grilled BBQ Chicken and Pineapple Rice Burrito Recipe',
                'description' => "Preparation
                1. In a pot, mix chicken thigh fillet, DEL MONTE Quick n Easy Barbecue Marinade, red onion, lemongrass and pepper together. Stew the chicken for 5 minutes.
                2. Remove chicken from the pot. Add cornstarch slurry in the marinade, simmer until thick then set aside.
                3. Grease grill pan with Contadina Pure Olive Oil and pan-grill chicken until fully cooked then set aside.
                4. In a separate grill pan, heat Contadina Pure Olive Oil then grill the vegetables. Once charred, slice into strips and set aside.
                5. For the Pininyahan Rice: Sauté garlic in Contadina Pure Olive Oil in a wok until golden brown then season with salt and pepper.
                6. Add cooked rice, DEL MONTE Crushed Pineapple and half of the sauce. Mix until well incorporated.
                7. Add parsley and mix well.
                8. Get 2 pieces of tortilla. Lay one piece over the other so that there is a 3-inch overlap. This will allow for a longer burrito. Place 1/2 cup of rice, 3 pieces of chicken, grilled bell peppers, onion, cheese, and bacon. Tuck both sides then roll forward to make a tight roll. Wrap with wax paper.
                 9. Heat burrito in a pan or grill pan over medium-high heat to slightly crisp tortilla. Serve with the remaining marinade for the sauce.",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'        => 'Shrimp Barbecue Recipe',
                'description' => "Preparation
                1. In a bowl, mix garlic, DEL MONTE Hot & Spicy Ketchup, and DEL MONTE Quick n Easy Barbecue Marinade. 
                2. Skewer the shrimps (4 pieces per skewer), then marinate for 30 minutes. 
                3. Drain the shrimps and reserve the marinade. Grill the shrimps in an oiled grill-pan. 
                4. In a pan, reduce the reserved marinade until thickened. 
                5. Glaze the shrimp skewers with sauce then serve.",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'        => 'Honey Curry Chicken Recipe',
                'description' => "Preparation
                1. Dip the chicken in cornstarch. Deep-fry then set aside.
                2. Deep-fry the frozen fries until golden brown. Drain and set aside.
                3. Mix the honey, DEL MONTE Quick 'n Easy Curry Mix. Toss in the chicken.",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'        => 'Grilled Honey BBQ Pork',
                'description' => "Preparation
                1. Mix DEL MONTE Quick n Easy Barbecue Marinade, DEL MONTE Red Cane Vinegar, honey, and pepper. Marinate the pork chops for 1 hour.
                2. After marinating, drain the pork then reserve the marinade.
                3. Heat an oiled grill pan, then grill the pork chops.
                4. Heat the reserved marinade in a pot. Bring to a boil then simmer for 5 minutes.
                5. Pour over grilled pork chops then serve.",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name'        => 'Beef Kofta',
                'description' => "Preparation
                1. Mix all the ingredients in a bowl and allow to stand for 30 minutes.
                2. Form into balls.
                3. Grill.
                4. Serve with grilled tomato and buttered steamed rice.",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

    }
}
