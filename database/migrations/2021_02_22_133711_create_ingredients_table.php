<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        if (!Schema::hasTable('ingredients')) {
            Schema::create('ingredients', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('measure');
                $table->string('supplier');
                $table->softDeletes();
                $table->timestamps();
            }); 
        }

        if (!Schema::hasTable('recipes')) {
            Schema::create('recipes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->longtext('description');
                $table->softDeletes();
                $table->timestamps();
            });
        }

           
        if (!Schema::hasTable('ingredient_recipes')) {
            Schema::create('ingredient_recipes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('ingredient_id')->nullable();
                $table->unsignedBigInteger('recipe_id')->nullable();
                $table->integer('amount');
                $table->timestamps();

                $table->index(['id', 'ingredient_id']);
                $table->index(['recipe_id']);
                $table->foreign('ingredient_id')->references('id')->on('ingredients')->onDelete('cascade');
                $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
            });

        }

        if (!Schema::hasTable('boxes')) {
            Schema::create('boxes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamp('delivery_date');
                $table->longtext('remarks')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('recipe_boxes')) {
            Schema::create('recipe_boxes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('box_id')->nullable();
                $table->unsignedBigInteger('recipe_id')->nullable();
                $table->timestamps();

                $table->index(['id', 'box_id']);
                $table->index(['recipe_id']);
                $table->foreign('box_id')->references('id')->on('boxes')->onDelete('cascade');
                $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_recipes');
        Schema::dropIfExists('recipe_boxes');
        Schema::dropIfExists('boxes');
        Schema::dropIfExists('recipes');
        Schema::dropIfExists('ingredients');
    }
}
