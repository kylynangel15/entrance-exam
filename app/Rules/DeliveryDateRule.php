<?php

namespace App\Rules;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class DeliveryDateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validate = false;
        $current = now()->format('Y-m-d H:i:s');
        $new_date = Carbon::parse($current)
                            ->addDays(2);

        $value > $new_date ? $validate = true : $validate= false;
        return $validate;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Delivery Date must be after 2 days';
    }
}
