<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = [
        'name',
        'measure', 
        'supplier', 
    ];

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class, 'ingredient_recipes')
                    ->withPivot('amount');
    }

    public static function storeItem($request)
    {
        $me = new SELF();
        SELF::updateItem($request, $me);
        return $me;
    }

    public static function updateItem($request, $me)
    {
        $me->fill($request->all());
        $me->save();

        return $me;
    }
}
