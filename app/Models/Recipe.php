<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = [
        'name',
        'description', 
    ];  

    protected $with = ['ingredients'];


    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'ingredient_recipes')
                    ->withPivot('amount');
    }

    public function boxes()
    {
        return $this->belongsToMany(Box::class, 'recipe_boxes');
    }

    public static function storeItem($request)
    {
        $me = new SELF();
        SELF::updateItem($request, $me);
        return $me;
    }

    public static function updateItem($request, $me)
    {
        $me->fill($request->all());
        $me->save();

        if($request->isIngChange){
            $me->ingredients()->detach();
            foreach ($request->ingredients as $key => $ingredient) {
                $me->ingredients()
                    ->attach(
                        $ingredient['ingredient']['id'], 
                        ['amount'=> $ingredient['amount']]
                    );
            }
        }

        return $me;
    }
}
