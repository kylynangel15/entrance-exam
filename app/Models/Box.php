<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Box extends Model
{
    protected $fillable = [
        'delivery_date',
        'remarks', 
    ];  

    protected $with = ['recipes'];
    
    public function recipes()
    {
        return $this->belongsToMany(Recipe::class, 'recipe_boxes');
    }

    public static function storeItem($request)
    {
        $me = new SELF();
        SELF::updateItem($request, $me);
        return $me;
    }

    public static function updateItem($request, $me)
    {
        $me->fill($request->all());
        $me->save();

        $me->recipes()->detach();
        $me->recipes()->attach($request->recipes);
        
        return $me;
    }

    public static function validateRange($date_range)
    {
        $dates = $date_range;
        $compare_function = function($a,$b) {
            return strtotime($a) - strtotime($b);
        };
        usort($dates, $compare_function);

        if(count($dates) < 2) {
            $dates[1]  = Carbon::parse($dates[0])
                        ->addDays(365)->format('Y-m-d');
        }

        return $dates;
    }

}
