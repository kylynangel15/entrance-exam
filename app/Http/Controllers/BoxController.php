<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Box;
use App\Http\Requests\BoxRequest;

class BoxController extends Controller
{
    public function index(Request $request)
    {
        $dates = null;
        if($request->date_range)
          $dates = Box::validateRange($request->date_range);
        
        $boxes = Box::orderBy('delivery_date')
                    ->when($dates, function($q) use($dates) {
                        $q->whereDate('delivery_date','>=', $dates[0])
                        ->whereDate('delivery_date','<=',$dates[1]);
                    })
                    ->get();
        return response()->json($boxes);
    }

    public function store(BoxRequest $request)
    {
        Box::storeItem($request); 
    }

    public function update(BoxRequest $request, Box $box)
    {
        Box::updateItem($request, $box); 
    }


    public function destroy(Box $box)
    {
        $box->delete();
    }
}
