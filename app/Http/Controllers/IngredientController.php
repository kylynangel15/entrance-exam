<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Models\Recipe;
use App\Http\Requests\IngredientRequest;
use Carbon\Carbon;
use DB;
class IngredientController extends Controller
{
    
    public function index()
    {
        $ingredients = Ingredient::orderByDesc('updated_at')->get();
        return response()->json($ingredients);
    }

    public function store(IngredientRequest $request)
    {
        Ingredient::storeItem($request); 
    }

    public function update(IngredientRequest $request, Ingredient $ingredient)
    {
        Ingredient::updateItem($request, $ingredient); 
    }

    public function destroy(Ingredient $ingredient)
    {
        $ingredient->delete();
    }
    
    public function orderFilter(Request $request)
    {   
        $order_date = Carbon::parse($request->order_date);
        $end_date = Carbon::parse($request->order_date)->addDays(7);

        $ingredients =  Ingredient::join('ingredient_recipes', 'ingredients.id', '=', 'ingredient_recipes.ingredient_id')
                                    ->join('recipes', 'ingredient_recipes.recipe_id', '=', 'recipes.id')
                                    ->join('recipe_boxes', 'recipes.id', '=', 'recipe_boxes.recipe_id')
                                    ->join('boxes', 'recipe_boxes.box_id', '=', 'boxes.id')
                                    ->where('delivery_date','>=',$order_date)
                                    ->where('delivery_date','<=',$end_date)
                                    ->select('ingredients.*','ingredient_recipes.*',DB::raw("Sum(ingredient_recipes.amount) as total_amount"))
                                    ->groupBy('ingredients.name')
                                    ->get();   

        $recipes = Recipe::with(['boxes' => function($q) use($order_date, $end_date){
                            $q->where('delivery_date','>=',$order_date)
                                ->where('delivery_date','<=',$end_date);
                            }])->wherehas('boxes',function($q) use($order_date, $end_date){
                                $q->where('delivery_date','>=',$order_date)
                                ->where('delivery_date','<=',$end_date);
                            })->get();

        
        return response()->json([
                            'ingredients' => $ingredients,
                            'recipes' => $recipes,
                        ]);

    }
}
