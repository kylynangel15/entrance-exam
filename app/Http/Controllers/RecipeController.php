<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Http\Requests\RecipeRequest;

class RecipeController extends Controller
{
    
    public function index()
    {
        $recipes = Recipe::orderByDesc('updated_at')->get();
        return response()->json($recipes);
    }
  
    public function store(RecipeRequest $request)
    {
        Recipe::storeItem($request); 
    }

    public function update(RecipeRequest $request, Recipe $recipe)
    {
        Recipe::updateItem($request, $recipe); 
    }

    public function destroy(Recipe $recipe)
    {
        $recipe->delete();
    }
}
