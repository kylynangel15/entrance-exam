<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required",
            'description' => "required",
            'ingredients' => "required",
            'ingredients.*.amount' => "integer",
        ];
    }

    public function messages()
    {
        return [
            'ingredients.*.amount.integer' => 'The value must be an integer number',
        ];
    }
}
