<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'api/','as'=>'api.'], function(){
    Route::resource('ingredients', 'IngredientController');
    Route::resource('recipes', 'RecipeController');
    Route::resource('boxes', 'BoxController');
    Route::get('/order-filter', 'IngredientController@orderFilter')->name('order-filter');
});
Route::get('{any}', 'HomeController@index')->where('any','.*')->name('index');

